package {
    default_applicable_licenses: [
        "external_pandora_bt-test-interfaces_license",
    ],
}

// Added automatically by a large-scale-change
// See: http://go/android-license-faq
license {
    name: "external_pandora_bt-test-interfaces_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
    ],
    license_text: [
        "LICENSE",
    ],
}

java_library {
    name: "pandora-grpc-java",
    visibility: ["//packages/modules/Bluetooth/android/pandora/server"],
    srcs: [
        "pandora/*.proto",
    ],
    static_libs: [
        "grpc-java-lite",
        "guava",
        "javax_annotation-api_1.3.2",
        "libprotobuf-java-lite",
        "opencensus-java-api",
        "pandora-proto-java",
    ],
    proto: {
        include_dirs: [
            "external/protobuf/src",
            "external/pandora/bt-test-interfaces",
        ],
        plugin: "grpc-java-plugin",
        output_params: [
           "lite",
        ],
    },
}

java_library {
    name: "pandora-proto-java",
    visibility: [
        "//packages/modules/Bluetooth/android/pandora/server",
        "//packages/modules/Bluetooth/pandora/interfaces",
    ],
    srcs: [
        "pandora/*.proto",
        ":libprotobuf-internal-protos",
    ],
    static_libs: [
        "libprotobuf-java-lite",
    ],
    proto: {
        // Disable canonical path as this breaks the identification of
        // well known protobufs
        canonical_path_from_root: false,
        type: "lite",
        include_dirs: [
            "external/protobuf/src",
            "external/pandora/bt-test-interfaces",
        ],
    },
}

genrule {
    name: "protoc-gen-mmi2grpc-python-src-interfaces",
    srcs: ["python/_build/protoc-gen-custom_grpc"],
    cmd: "cp $(in) $(out)",
    out: ["protoc-gen-custom_grpc.py"],
}

python_binary_host {
    name: "protoc-gen-custom_grpc",
    main: "protoc-gen-custom_grpc.py",
    srcs: [":protoc-gen-mmi2grpc-python-src-interfaces"],
    libs: ["libprotobuf-python"],
}

genrule {
    name: "pandora-python-src",
    tools: [
        "aprotoc",
        "protoc-gen-custom_grpc"
    ],
    cmd: "$(location aprotoc)" +
         "    -Iexternal/pandora/bt-test-interfaces" +
         "    -Iexternal/protobuf/src" +
         "    --plugin=protoc-gen-grpc=$(location protoc-gen-custom_grpc)" +
         "    --grpc_out=$(genDir)" +
         "    --python_out=$(genDir)" +
         "    $(in)",
    srcs: [
        "pandora/asha.proto",
        "pandora/a2dp.proto",
        "pandora/host.proto",
        "pandora/security.proto",
    ],
    out: [
        "pandora/asha_grpc.py",
        "pandora/asha_pb2.py",
        "pandora/a2dp_grpc.py",
        "pandora/a2dp_pb2.py",
        "pandora/host_grpc.py",
        "pandora/host_pb2.py",
        "pandora/security_grpc.py",
        "pandora/security_pb2.py",
    ]
}

python_library_host {
    name: "pandora-python",
    srcs: [
        ":pandora-python-src",
    ],
}
